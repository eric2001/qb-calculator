# QB Calculator
![QB Calculator](./screenshot.jpg) 

## Description
This is a basic 4 function calculator, written in QBasic. 

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
Run the program.  Enter in numbers and select math functions either by using the corresponding keys on the keyboard or by clicking the screen with the mouse.

## History
(Dates are estimates based on File Creation and Last Modified dates and may be inaccurate)

This project was started on 03 January 1999.

This project was last updated on 22 January 2001.

Download: [Binary](./bin/QBCalculatorBinary.zip) | [Source](./bin/QBCalculatorSource.zip)
